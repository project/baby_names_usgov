<?php

/**
 * @file
 * Define main functionality for Baby Names from US Gov
 */

/**
 * The first year with census data.
 */
define('BABY_NAMES_USGOV_DATA_FIRST_YEAR', 1880);

/**
 * The last year with census data.
 */
define('BABY_NAMES_USGOV_DATA_LAST_YEAR', 2015);

/**
 * The last year with census data total.
 */
define('BABY_NAMES_USGOV_DATA_LAST_YEAR_TOTAL', 3668183);

/**
 * Return all count data for a name (and optionally a gender).
 *
 * @param string $name
 *   The name we are looking up.
 * @param string $gender
 *   (optional) The gender "F" or "M". If not provided, all results are found.
 * @return string
 *   Returns a string containing the JSON representation of the data counts
 *   formatted to work for a Chartist JS chart.
 */
function baby_names_usgov_counts_get($name, $gender = NULL) {
  $cache_id = 'baby_names_usgov_birth_counts_' . $name;
  $json = &drupal_static(__FUNCTION__);
  if (!isset($json)) {
    if ($cache = cache_get($cache_id, 'cache_baby_names_usgov')) {
      $json = $cache->data;
    }
    else {
      $query = db_select('baby_names_usgov_names', 'b')
        ->condition('name', $name);
      $query->join('baby_names_usgov_counts', 'c', 'c.bnid = b.bnid');
      $query->fields('c', array('year', 'count'));
      $query->fields('b', array('gender'));
      if (!is_null($gender)) {
        $query->condition('gender', $gender);
      }
      $query->orderBy('gender');
      $query->orderBy('c.year');
      $results = $query->execute()->fetchAll();

      $data = [];
      if (!empty($results)) {
        $males = [];
        $females = [];

        for ($year = BABY_NAMES_USGOV_DATA_FIRST_YEAR; $year <= BABY_NAMES_USGOV_DATA_LAST_YEAR; $year++) {
          $data['labels'][] = $year;
          $males[] = 0;
          $females[] = 0;
        }

        foreach ($results as $result) {
          $year_index = $result->year - BABY_NAMES_USGOV_DATA_FIRST_YEAR;
          switch ($result->gender) {
            case 'M':
              $males[$year_index] = (int) $result->count;
              break;
            case 'F':
              $females[$year_index] = (int) $result->count;
              break;
          }
        }

        switch ($gender) {
          case 'M':
            $data['series'][] = [
              'gender' => 'M',
              'data' => $males
            ];
            break;
          case 'F':
            $data['series'][] = [
              'gender' => 'F',
              'data' => $females
            ];
            break;
          default:
            $data['series'][] = [
              'gender' => 'M',
              'data' => $males
            ];
            $data['series'][] = [
              'gender' => 'F',
              'data' => $females
            ];
            break;
        }
      }

      $json = json_encode($data);
      cache_set($cache_id, $json, 'cache_baby_names_usgov');
    }
  }
  return $json;
}

/**
 * Return a Baby Name ID (bnid) when provided a name and gender.
 *
 * @param string $name
 *   The name we are looking up.
 * @param string $gender
 *   The gender "F" or "M".
 * @return int
 *   The unique baby name ID for this name and gender pairing.
 */
function baby_names_usgov_name_id_get($name, $gender) {
  return db_select('baby_names_usgov_names', 'b')
    ->fields('b', array('bnid'))
    ->condition('name', $name)
    ->condition('gender', $gender)
    ->execute()
    ->fetchField();
}

/**
 * Adds baby name counts intelligently to database tables.
 *
 * If the name and gender the count is being added for doesn't already exist in
 * the names database, we add it first, then proceed to add the count. We are
 * not currently updating the totals table with this addition.
 *
 * @param string $name
 *   The name we are adding a count for.
 * @param string $gender
 *   The gender "F" or "M".
 * @param int $count
 *   The number of babies with this name for the year.
 * @param int $year
 *   The year this data if for.
 */
function _baby_names_usgov_count_add($name, $gender, $count, $year) {
  $bnid = baby_names_usgov_name_id_get($name, $gender);
  // Does this name exist already or does it need to be added?
  if (!$bnid) {
    $bnid = _baby_names_usgov_name_add($name, $gender);
  }
  db_insert('baby_names_usgov_counts')
    ->fields(array(
      'bnid' => $bnid,
      'year' => $year,
      'count' => $count,
    ))
    ->execute();
}

/**
 * Adds new baby name to database table.
 *
 * @param string $name
 *   The name we are adding.
 * @param string $gender
 *   The gender "F" or "M" we are adding.
 * @return int
 *   The unique baby name ID for this name and gender pairing.
 */
function _baby_names_usgov_name_add($name, $gender) {
  return db_insert('baby_names_usgov_names')
    ->fields(array(
      'name' => $name,
      'gender' => $gender,
    ))
    ->execute();
}
